<?php

session_start();

if(isset($_SESSION['username'])){
    header('location: login.php');
}

if(isset($_GET['logout'])){
    session_destroy();
    unset($_SESSION['username']);
    header('location: login.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Page</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
</head>
<body>

    <?php
        if(isset($_SESSION['success'])) :
    ?>
    <section class="container-hero">
        <?php if(isset($_SESSION['username'])) : ?>

        <h1>Hello, <span><?php echo $_SESSION['username']; ?></span></h1>
        <a href="index.php?logout=1" class="link-01">Logout</a>
    </section>
    
</body>
</html>