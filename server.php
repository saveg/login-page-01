<?php

// Starting Tracking Session
session_start();

// Initialising Variables

$username = "";
$password = "";

$errors = array();

// Connect to DataBase
// mysqli_connect -> ("IP-ADDRESS", "USER-TO-LOGIN", "PASSWORD", "DATABASE-TO-TARGET")
$db = mysqli_connect("localhost", "root", "", "login-page") or die("Could not connect to database");

// Register User

$username = mysqli_real_escape_string($db, $_POST['username-input']);
$password_1 = mysqli_real_escape_string($db, $_POST['password-input1']);
$password_2 = mysqli_real_escape_string($db, $_POST['password-input2']);

// Form Validation

if(empty($username)) {
    array_push($errors, "Username is required");
    }
    if(empty($password_1)) {
        array_push($errors, "Password is required");           
        }
        if($password_1 != $password_2){
            array_push($errors, "Passwords do not match");
        }

// Checking Database for existing users (can't have have usernames)

$user_check_query = "SELECT * FROM user WHERE username = '$username' LIMIT 1";

$results = mysqli_query($db, $user_check_query);
$user = mysqli_fetch_assoc($results);

    if ($user){
        if($user['username'] === $username){
            array_push($errors, 'Username already exists');
        }
    }

// Register User with no Error

if(count($errors) == 0){
    $password = md5(password_1); //Encrypting the password 
    $query = "INSERT INTO user (username, password) VALUES ('$username', '$password')";

    mysqli_query($db, $query); //Running the query in the database
    $_SESSION['username'] = $username;
    $_SESSION['success'] = "You are now logged in";

    header('location: index.php');
}



?>