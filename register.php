<?php include('server.php') ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Page</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
</head>
<body>

    <section class="container">
        <form class="login-wrapper" method="POST" action="register.php">

                <img src="imgs/Login.svg" alt="Login Icon">
                <h1>Sign Up</h1>

            <div class="input-wrapper">
                
                <label for="username">Username</label>
                <input type="text" id="username" name="username-input" placeholder="Enter username" required>

                <label for="password">Password</label>
                <input type="password" id="password" name="password-input1" placeholder="Enter password" required>

                <label for="password">Password</label>
                <input type="password" id="password" name="password-input2" placeholder="Repeat password" required>

                <input type="submit" id="submit" value="Sign Up" name="signup-submit">

                <a href="login.php" class="link-01">Already have an account?</a>
            </div>

        </form>
    </section>
    
</body>
</html>